var swiper = new Swiper('.slide-programs', {
	slidesPerView: 4,
	spaceBetween: 30,
	pagination: {
		el: '.pagination-programs',
		clickable: true,
	},
	breakpoints: {
		1024: {
			slidesPerView: 3,
			spaceBetween: 40,
		},
		768: {
			slidesPerView: 2,
			spaceBetween: 30,
		},
		640: {
			slidesPerView: 1,
			spaceBetween: 20,
		},
		320: {
			slidesPerView: 1,
			spaceBetween: 10,
		}
	}
});