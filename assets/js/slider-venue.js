   var swiper = new Swiper('.slider-venues', {
     slidesPerView: 3,
     spaceBetween: 10,
    //  freeMode: true,
     pagination: {
       el: '.pagination-venue',
       clickable: true,
     },
     breakpoints: {
       1024: {
         slidesPerView: 3,
         spaceBetween: 40,
       },
       768: {
         slidesPerView: 2,
         spaceBetween: 30,
       },
       640: {
         slidesPerView: 1,
         spaceBetween: 20,
       },
       320: {
         slidesPerView: 1,
         spaceBetween: 10,
       }
     }
   });