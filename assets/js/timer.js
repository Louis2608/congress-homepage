const second = 1000,
    minute = second * 60,
    hour = minute * 60,
    day = hour * 24;

let countDown = new Date('Nov 8, 2018 09:30:00').getTime(),
    x = setInterval(function () {

        let now = new Date().getTime(),
            distance = countDown - now;

        document.getElementById('value-days').innerText = Math.floor(distance / (day)),
        document.getElementById('value-hours').innerText = Math.floor((distance % (day)) / (hour)),
        document.getElementById('value-minutes').innerText = Math.floor((distance % (hour)) / (minute)),
        document.getElementById('value-seconds').innerText = Math.floor((distance % (minute)) / second);

        //do something later when date is reached
        //if (distance < 0) {
        //  clearInterval(x);
        //  'IT'S MY BIRTHDAY!;
        //}

    }, second)